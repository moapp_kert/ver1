package com.homework.a2013105031.earthquakeshelter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void gotoshelter(View v){
        Intent intent1 = new Intent(MainActivity.this, ShelterActivity.class);
        startActivity(intent1);
    }

    public void gotoinformation(View v){
        Intent intent2 = new Intent(MainActivity.this, EarthquakeHistoryActivity.class);
        startActivity(intent2);
    }

    public void gototips(View v){
        Intent intent3 = new Intent(MainActivity.this, tips.class);
        startActivity(intent3);
    }

    public void gotoquiz(View v){
        Intent intent4 = new Intent(MainActivity.this, quiz.class);
        startActivity(intent4);
    }

    public void onClickRelative(View v){
        Intent intent5 = new Intent(MainActivity.this, RelativePage.class);
        startActivity(intent5);
    }

    public void onClickCreatedby(View v){
        Intent intent6 = new Intent(MainActivity.this, createdby.class);
        startActivity(intent6);
    }
}
