package com.homework.a2013105031.earthquakeshelter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class quiz_ox extends AppCompatActivity {
    private int count_quiz=0;
    private quizlist q;
    private int value =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_ox);

        q = new quizlist();
        RadioGroup ColGroup = (RadioGroup)findViewById(R.id.yn_radio);
        ColGroup.setOnCheckedChangeListener(mRadioCheck);

        q = new quizlist();

        TextView qindexText = (TextView)findViewById(R.id.quiznumber);
        qindexText.setText(Integer.toString(count_quiz+1) + "번문제.");

        TextView qquestionText = (TextView)findViewById(R.id.question_yn);
        qquestionText.setText(q.getQStringlist().get(count_quiz));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == R.id.action_button)
            finish();
        return super.onOptionsItemSelected(item);
    }

    RadioGroup.OnCheckedChangeListener mRadioCheck =
            new RadioGroup.OnCheckedChangeListener(){
                public void onCheckedChanged(RadioGroup group, int checkedId){
                    if (group.getId() == R.id.yn_radio){
                        switch (checkedId){
                            case R.id.r_yes:
                                value = 1;
                                break;
                            case R.id.r_no:
                                value = 2;
                                break;

                        }
                    }
                }
            };

    public int onclick_yn_submit(View v)
    {
        if (q.getAStringlist().get(count_quiz) == value)
        {
            if(count_quiz == q.getAStringlist().size()-1)
            {
                Toast.makeText(getApplicationContext(), "다 풀었습니다.", Toast.LENGTH_SHORT).show();
                finish();
                return 0;
            }

            count_quiz++;

            TextView qindexText = (TextView) findViewById(R.id.quiznumber);
            qindexText.setText(Integer.toString(count_quiz+1) + "번문제.");

            TextView qquestionText = (TextView) findViewById(R.id.question_yn);
            qquestionText.setText(q.getQStringlist().get(count_quiz));

            RadioGroup ColGroup = (RadioGroup)findViewById(R.id.yn_radio);
            ColGroup.clearCheck();


            value =0;

            return 0;
        }
        else
        {
            Toast.makeText(getApplicationContext(), "틀렸습니다", Toast.LENGTH_SHORT).show();
            return 0;
        }
    }
}