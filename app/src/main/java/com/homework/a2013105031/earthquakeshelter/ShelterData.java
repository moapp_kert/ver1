package com.homework.a2013105031.earthquakeshelter;

/**
 * Created by woochan on 2017-12-16.
 */

public class ShelterData {
    private String Name;
    private String Adress;
    private double latitude;
    private double longitude;
    private String tag; // 옥외대피소 out, 실내구호소 in

    public ShelterData(String Name, String Adress, double latitude, double longitude, String tag){
        this.Name = Name;
        this.Adress = Adress;
        this.latitude = latitude;
        this.longitude = longitude;
        this.tag = tag;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAdress() {
        return Adress;
    }

    public void setAdress(String adress) {
        Adress = adress;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
