package com.homework.a2013105031.earthquakeshelter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class tip_place extends AppCompatActivity {
    private int index=0;
    private int a[] = {R.drawable.img2_1,R.drawable.img2_2,R.drawable.img2_3,R.drawable.img2_4,R.drawable.img2_5,R.drawable.img2_6,R.drawable.img2_7,R.drawable.img2_8,R.drawable.img2_9};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tip_place);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView imageview = (ImageView)findViewById(R.id.imageView_pla);
        imageview.setImageResource(a[index]);

        TextView textview = (TextView)findViewById(R.id.textView_index_pla);
        textview.setText(Integer.toString(index+1)+"/"+Integer.toString(a.length));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == R.id.action_button)
            finish();
        return super.onOptionsItemSelected(item);
    }

    public int Onclickgoforward_pla(View v)
    {
        if(index < a.length-1)
        {
            index++;
        }
        else
        {
            return 0;
        }

        ImageView imageview = (ImageView)findViewById(R.id.imageView_pla);
        imageview.setImageResource(a[index]);

        TextView textview = (TextView)findViewById(R.id.textView_index_pla);
        textview.setText(Integer.toString(index+1)+"/"+Integer.toString(a.length));

        return 0;
    }

    public int Onclickback_pla(View v)
    {
        if(index > 0)
        {
            index--;
        }
        else
        {
            return 0;
        }

        ImageView imageview = (ImageView)findViewById(R.id.imageView_pla);
        imageview.setImageResource(a[index]);

        TextView textview = (TextView)findViewById(R.id.textView_index_pla);
        textview.setText(Integer.toString(index+1)+"/"+Integer.toString(a.length));
        return 0;
    }
}
