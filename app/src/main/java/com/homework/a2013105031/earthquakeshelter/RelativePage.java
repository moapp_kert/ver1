package com.homework.a2013105031.earthquakeshelter;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class RelativePage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relative_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == R.id.action_button)
            finish();
        return super.onOptionsItemSelected(item);
    }

    public void onclickmeteo(View v){
        Intent intent1 = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.kma.go.kr/weather/earthquake_volcano/report.jsp"));
        startActivity(intent1);
    }

    public void onclicksei(View v){
        Intent intent2 = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.aurum.re.kr/KoreaEqk/SelfChkMain"));
        startActivity(intent2);
    }

    public void onclicknsa(View v){
        Intent intent3 = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.mpss.go.kr/home/safetys/disasterInfo/disasterTypeInfo1/0003"));
        startActivity(intent3);
    }
}