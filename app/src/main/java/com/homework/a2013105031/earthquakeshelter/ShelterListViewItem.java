package com.homework.a2013105031.earthquakeshelter;

import android.graphics.drawable.Drawable;

/**
 * Created by woochan on 2017-12-15.
 */

public class ShelterListViewItem {
    private Drawable image;
    private String name;
    private String address;
    private double latitude;
    private double longitude;

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
