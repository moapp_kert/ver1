package com.homework.a2013105031.earthquakeshelter;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ShelterActivity extends AppCompatActivity implements LocationListener, View.OnClickListener {

    public final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    public final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 2;

    TextView addressTextView;
    Button currentLocationButton;
    ListView shelterListView;

    ShelterListViewAdapter shelterListViewAdapter;

    LocationManager locationManager = null;
    String provider = null;

    double latitude, longitude;

    // 공공데이터 수집

    String END_TAG = "EarthquakeIndoors";
    boolean data_end = false;

    String urlData = "http://data.mpss.go.kr/openapi/EarthquakeIndoors"; // 실내구호소 공공데이터
    String urlData2 = "http://data.mpss.go.kr/openapi/EarthquakeOutdoorsShelter"; // 옥외구호소 공공데이터
    String urlKey = "OULAU457420171213042243VOVVT"; // 실내구호소 공공데이터 인증키
    String urlKey2 = "CVPCI457420171213042047UYNGQ"; // 옥외구호소 공공데이터 인증키
    int urlIndex = 1;
    String _url = urlData + "?KEY=" + urlKey + "&pIndex=" + urlIndex + "&pSize=1000";

    ArrayList<ShelterData> arrayList = new ArrayList<>();
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shelter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // 권한 처리
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
            else {
                ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
        }

        addressTextView = (TextView)findViewById(R.id.textview_longitude);

        currentLocationButton = (Button)findViewById(R.id.current_location_button);
        currentLocationButton.setOnClickListener(this);

        shelterListView = (ListView)findViewById(R.id.listview_shelter);
        shelterListViewAdapter = new ShelterListViewAdapter();
        shelterListView.setAdapter(shelterListViewAdapter);
        shelterListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:37.5, 127.0?q=PLACE_NAME"));
                String gps = "geo:" + shelterListViewAdapter.getItem(i).getLatitude() + ", " + shelterListViewAdapter.getItem(i).getLongitude();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(gps));
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == R.id.action_button)
            finish();
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }
                else {
                    Toast.makeText(this, "권한 사용을 동의해주십시오.", Toast.LENGTH_SHORT).show();
                    finish();
                }
        }
    }

    private void setLocationManager()
    {
        locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, true);
        if (provider == null || !locationManager.isProviderEnabled(provider)) {
            List<String> list = locationManager.getAllProviders();

            for (int i = 0; i < list.size(); i++) {
                String temp = list.get(i);

                if (locationManager.isProviderEnabled(temp)) {
                    provider = temp;
                    break;
                }
            }
        }

        Location location = locationManager.getLastKnownLocation(provider);
        if (location == null) {
            Toast.makeText(this, "사용 가능한 위치 정보 제공자가 없습니다.", Toast.LENGTH_SHORT).show();
        }
        else {
            onLocationChanged(location);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //locationManager.requestLocationUpdates(provider, 500, 1, this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        //locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    public String getAddress()
    {
        String address = null;

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());

        //주소 목록을 담기 위한 HashMap
        List<Address> list = null;

        try{
            list = geocoder.getFromLocation(latitude, longitude, 1);
        }
        catch(Exception e){
            e.printStackTrace();
        }

        if(list == null){
            Log.e("getAddress", "주소 데이터 얻기 실패");
            return null;
        }

        if(list.size() > 0){
            Address addr = list.get(0);
            address = addr.getCountryName() + " "
                    + addr.getPostalCode() + " "
                    + addr.getLocality() + " "
                    + addr.getThoroughfare() + " "
                    + addr.getFeatureName();
        }

        return address;
    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.current_location_button:
                setCurrentLocationButton();
                break;
        }
    }

    private void setCurrentLocationButton()
    {
        // GPS 여부를 확인한다
        if ( checkGPS() ) {
            // 현재 위치를 읽는다
            setLocationManager();
            addressTextView.setText(getAddress());
            // 백그라운드에서 대피소 정보를 읽는다
            init();
        }
    }

    private void init() {
        dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("데이터 수집 중...");
        dialog.show();
        new BackgroundTask().execute();
    }

    private class BackgroundTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0) {
            Log.d("background_progress", "start");
            // 한 페이지 당 1000개씩 3페이지의 실내구호소 데이터를 읽는다
            for (urlIndex = 1; urlIndex <= 3; urlIndex++) {
                try {
                    _url = urlData + "?KEY=" + urlKey + "&pIndex=" + urlIndex + "&pSize=1000";
                    URL url = new URL(_url);
                    XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser parser = factory.newPullParser();
                    parser.setInput(new BufferedInputStream(url.openStream()), "utf-8");

                    String tag = null; // 태그

                    String shelter_name = null; // vt_acmdfclty_nm : 시설명
                    String shelter_address = null; // dtl_adres : 주소
                    String shelter_xcord = null; // xcord : 경도
                    String shelter_ycord = null; // ycord : 위도

                    int type = parser.getEventType(); // 태그의 종류를 읽어온다
                    while (type != XmlPullParser.END_DOCUMENT) {
                        tag = parser.getName(); // 태그 이름을 읽어온다
                        switch (type) {
                            case XmlPullParser.START_TAG: {
                                if (tag.equals("vt_acmdfclty_nm")) {
                                    parser.next();
                                    shelter_name = parser.getText();
                                    parser.next();
                                } else if (tag.equals("dtl_adres")) {
                                    parser.next();
                                    shelter_address = parser.getText();
                                    parser.next();
                                } else if (tag.equals("xcord")) {
                                    parser.next();
                                    shelter_xcord = parser.getText();
                                    parser.next();
                                } else if (tag.equals("ycord")) {
                                    parser.next();
                                    shelter_ycord = parser.getText();
                                    parser.next();
                                }

                                break;
                            }
                            case XmlPullParser.END_TAG: {
                                if (tag.equals("row")) { // 데이터 row 하나에 대해 컬럼값을 읽기 시작한다.
                                    publishProgress(shelter_name, shelter_address, shelter_xcord, shelter_ycord, "in");
                                }
                                break;
                            }
                        }
                        type = parser.next();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("background_progress", e.getMessage());
                }
            }

            // 한 페이지 당 1000개씩 9페이지의 옥외대피소 데이터를 읽는다
            for (urlIndex = 1; urlIndex <= 9; urlIndex++) {
                try {
                    _url = urlData2 + "?KEY=" + urlKey2 + "&pIndex=" + urlIndex + "&pSize=1000";
                    URL url = new URL(_url);
                    XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                    factory.setNamespaceAware(true);
                    XmlPullParser parser = factory.newPullParser();
                    parser.setInput(new BufferedInputStream(url.openStream()), "utf-8");

                    String tag = null; // 태그

                    String shelter_name = null; // vt_acmdfclty_nm : 시설명
                    String shelter_address = null; // dtl_adres : 주소
                    String shelter_xcord = null; // xcord : 경도
                    String shelter_ycord = null; // ycord : 위도

                    int type = parser.getEventType(); // 태그의 종류를 읽어온다
                    while (type != XmlPullParser.END_DOCUMENT) {
                        tag = parser.getName(); // 태그 이름을 읽어온다
                        switch (type) {
                            case XmlPullParser.START_TAG: {
                                if (tag.equals("vt_acmdfclty_nm")) {
                                    parser.next();
                                    shelter_name = parser.getText();
                                }
                                if (tag.equals("dtl_adres")) {
                                    parser.next();
                                    shelter_address = parser.getText();
                                }
                                if (tag.equals("xcord")) {
                                    parser.next();
                                    shelter_xcord = parser.getText();
                                }
                                if (tag.equals("ycord")) {
                                    parser.next();
                                    shelter_ycord = parser.getText();
                                }
                                break;
                            }
                            case XmlPullParser.END_TAG: {
                                if (tag.equals("row")) { // 데이터 row 하나에 대해 컬럼값을 읽기 시작한다.
                                    publishProgress(shelter_name, shelter_address, shelter_xcord, shelter_ycord, "out");
                                }
                                break;
                            }
                        }
                        type = parser.next();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("background_progress", e.getMessage());
                }
            }
            return _url;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            //Log.d("background_progress", values[0] + " " + values[1] + " " + values[2] + " " + values[3]);
            if (values[2] == null || values[3] == null) {
                return;
            }
            if (isNearWithMe(Double.parseDouble(values[3]), Double.parseDouble(values[2])))
                arrayList.add(new ShelterData(values[0], values[1], Double.parseDouble(values[3]), Double.parseDouble(values[2]), values[4]));
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            // 현재 위치가 아니라 파싱된 대피소 목록을 넣어야함
            shelterListViewAdapter.clearItemList();
            for (ShelterData shelterData : arrayList) {
                if (shelterData.getTag().equals("in"))
                    shelterListViewAdapter.addItem(getDrawable(R.drawable.indoor), shelterData.getName(), shelterData.getAdress(), shelterData.getLatitude(), shelterData.getLongitude());
                else
                    shelterListViewAdapter.addItem(getDrawable(R.drawable.outdoor), shelterData.getName(), shelterData.getAdress(), shelterData.getLatitude(), shelterData.getLongitude());
            }
            shelterListViewAdapter.notifyDataSetChanged();

            dialog.dismiss();
            Toast.makeText(ShelterActivity.this, "Background progress end", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isNearWithMe(double latitude, double longitude) {
        float[] results = new float[1];

        Location.distanceBetween(this.latitude, this.longitude, latitude, longitude, results);

        if (results[0] <= 0)
            return false;

        if (results[0] < 2000) { // 1km 이내면 true
            return true;
        } else {
            return false;
        }
    }

    public boolean checkGPS() { // GPS 사용 여부를 체크한다
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean isGPS = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        if (isGPS) {
            return true;
        } else { // 만약 꺼져있다면 설정 창을 띄운다
            Toast.makeText(this, "GPS가 꺼져있습니다", Toast.LENGTH_LONG).show();
            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), 0);
        }
        return false;

    }
}
