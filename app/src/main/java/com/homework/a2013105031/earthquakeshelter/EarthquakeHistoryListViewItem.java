package com.homework.a2013105031.earthquakeshelter;

import android.graphics.drawable.Drawable;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by woochan on 2017-12-15.
 */

public class EarthquakeHistoryListViewItem {
    private Drawable image;

    private String location; // 위치
    private String time; // 시간(년월일시분)

    private String fcTp; // 통보 종류(미리 문자열로 해석해서 추가) 1 : 국내 지진정보 2 : 국외 지진정보 3 : 국내 지진통보 4 : 국외 지진통보 5 : 지진 정밀분석
    private String rem; // 참고사항
    private double latitude; // 위도
    private double longitude; // 경도
    private double mt; // 규모
    private String imgUrl; // 발생위치 이미지 url

    public void setImage(Drawable image) {
        this.image = image;
    }

    public Drawable getImage() {
        return image;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLocation() {
        return location;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        String oldString = time;
        String newString = null;
        try {
            Date date = new SimpleDateFormat("yyyyMMddHHmm").parse(oldString);
            newString = new SimpleDateFormat("yyyy년 MM월 dd일 HH시 mm분").format(date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newString;
    }

    public void setFcTp(String fcTp) {
        this.fcTp = fcTp;
    }

    public String getFcTp() {
        return fcTp;
    }

    public void setRem(String rem) {
        this.rem = rem;
    }

    public String getRem() {
        return rem;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setMt(double mt) {
        this.mt = mt;
    }

    public double getMt() {
        return mt;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }
}
