package com.homework.a2013105031.earthquakeshelter;

import java.util.ArrayList;

public class quizlist {
    private ArrayList<String> QStringlist;
    private ArrayList<Integer> AStringlist;

    public quizlist()
    {
        QStringlist = new ArrayList<String>();
        AStringlist = new ArrayList<Integer>();

        QStringlist.add("지진 발생시에는 집에 있는 튼튼한 테이블 밑에 들어가 방석 등으로 머리를 보호하도록 한다.");
        AStringlist.add(1);

        QStringlist.add("지진을 대비해 문을 열어 출구를 확보하고, 만일 갇힐 사태를 대비하여 대피방법에 관해 미리 준비한다.");
        AStringlist.add(1);

        QStringlist.add("지진발생시에는 엘리베이터를 사용하며  만일 갇혔을 경우는 인터폰으로 구조를 요청한다.");
        AStringlist.add(2);

        QStringlist.add("해안에서 진동을 느꼈을 경우나 지진해일 경보를 들었을 경우 즉시 높은 곳으로 대피합시다.");
        AStringlist.add(1);

        QStringlist.add("해안에 가까울수록 위험하므로 해일이 발생하면 해안에서 멀리 떨어진 급경사가 없고 지형이 높은 안전한 곳으로 이동합니다.");
        AStringlist.add(1);
    }

    public ArrayList<String> getQStringlist()
    {
        return QStringlist;
    }
    public ArrayList<Integer> getAStringlist()
    {
        return AStringlist;
    }
}
