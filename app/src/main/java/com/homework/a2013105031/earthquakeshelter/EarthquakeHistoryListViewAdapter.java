package com.homework.a2013105031.earthquakeshelter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by woochan on 2017-12-15.
 */

public class EarthquakeHistoryListViewAdapter extends BaseAdapter {
    private ArrayList<EarthquakeHistoryListViewItem> itemList = new ArrayList<>();

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final int position = i;
        final Context context = viewGroup.getContext();

        // "listview_item" Layout을 inflate하여 view 참조 획득
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listview_item_earthquake_history, viewGroup, false);
        }

        // 화면에 표시될 View(Layout이 inflate된)으로부터 위젯에 대한 참조 획득
        ImageView iconImageView = (ImageView)view.findViewById(R.id.imageview_icon);
        TextView locationTextView = (TextView)view.findViewById(R.id.textview_location);
        TextView timeTextView = (TextView) view.findViewById(R.id.textview_time);

        // Data Set(listViewItemList)에서 position에 위치한 데이터 참조 획득
        EarthquakeHistoryListViewItem earthquakeHistoryListViewItem = itemList.get(position);

        // 아이템 내 각 위젯에 데이터 반영
        iconImageView.setImageDrawable(earthquakeHistoryListViewItem.getImage());
        locationTextView.setText(earthquakeHistoryListViewItem.getLocation());
        timeTextView.setText(earthquakeHistoryListViewItem.getTime());

        return view;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public EarthquakeHistoryListViewItem getItem(int i) {
        return itemList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void addItem(Drawable image, String location, String time, double latitude, double longitude) {
        EarthquakeHistoryListViewItem earthquakeHistoryListViewItem = new EarthquakeHistoryListViewItem();

        // 너무많다;;;


        itemList.add(earthquakeHistoryListViewItem);
    }

    public void addItem(EarthquakeHistoryListViewItem earthquakeHistoryListViewItem) {
        itemList.add(earthquakeHistoryListViewItem);
    }

    public void clearItemList() {
        itemList.clear();
    }
}
