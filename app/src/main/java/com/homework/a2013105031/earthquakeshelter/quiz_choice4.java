package com.homework.a2013105031.earthquakeshelter;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class quiz_choice4 extends AppCompatActivity {
    private int count_quiz=0;
    private quizdata q;
    private int value =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_choice4);

        RadioGroup ColGroup = (RadioGroup)findViewById(R.id.four_content);
        ColGroup.setOnCheckedChangeListener(mRadioCheck);

        q = new quizdata();

        TextView qindexText = (TextView)findViewById(R.id.quiznumber_4);
        qindexText.setText(Integer.toString(count_quiz+1) + "번문제.");

        TextView qquestionText = (TextView)findViewById(R.id.question_4);
        qquestionText.setText(q.getQStringlist().get(count_quiz));

        RadioButton radiobutton1 = (RadioButton)findViewById(R.id.radio0);
        radiobutton1.setText(q.getC1Stringlist().get(count_quiz));
        RadioButton radiobutton2 = (RadioButton)findViewById(R.id.radio1);
        radiobutton2.setText(q.getC2Stringlist().get(count_quiz));
        RadioButton radiobutton3 = (RadioButton)findViewById(R.id.radio2);
        radiobutton3.setText(q.getC3Stringlist().get(count_quiz));
        RadioButton radiobutton4 = (RadioButton)findViewById(R.id.radio3);
        radiobutton4.setText(q.getC4Stringlist().get(count_quiz));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == R.id.action_button)
            finish();
        return super.onOptionsItemSelected(item);
    }

    RadioGroup.OnCheckedChangeListener mRadioCheck =
            new RadioGroup.OnCheckedChangeListener(){
                public void onCheckedChanged(RadioGroup group, int checkedId){
                    if (group.getId() == R.id.four_content){
                        switch (checkedId){
                            case R.id.radio0:
                                value = 1;
                                break;
                            case R.id.radio1:
                                value = 2;
                                break;
                            case R.id.radio2:
                                value = 3;
                                break;
                            case R.id.radio3:
                                value = 4;
                                break;
                        }
                    }
                }
            };

    public int onclick_four_submit(View v)
    {
        if (q.getAStringlist().get(count_quiz) == value)
        {
            if(count_quiz == q.getAStringlist().size()-1)
            {
                Toast.makeText(getApplicationContext(), "다 풀었습니다.", Toast.LENGTH_SHORT).show();
                finish();
                return 0;
            }

            count_quiz++;

            TextView qindexText = (TextView) findViewById(R.id.quiznumber_4);
            qindexText.setText(Integer.toString(count_quiz+1)+"번문제.");

            TextView qquestionText = (TextView) findViewById(R.id.question_4);
            qquestionText.setText(q.getQStringlist().get(count_quiz));

            RadioButton radiobutton1 = (RadioButton)findViewById(R.id.radio0);
            radiobutton1.setText(q.getC1Stringlist().get(count_quiz));

            RadioButton radiobutton2 = (RadioButton)findViewById(R.id.radio1);
            radiobutton2.setText(q.getC2Stringlist().get(count_quiz));

            RadioButton radiobutton3 = (RadioButton)findViewById(R.id.radio2);
            radiobutton3.setText(q.getC3Stringlist().get(count_quiz));

            RadioButton radiobutton4 = (RadioButton)findViewById(R.id.radio3);
            radiobutton4.setText(q.getC4Stringlist().get(count_quiz));

            RadioGroup ColGroup = (RadioGroup)findViewById(R.id.four_content);
            ColGroup.clearCheck();
            value =0;

            return 0;
        }
        else
        {
            Toast.makeText(getApplicationContext(), "틀렸습니다", Toast.LENGTH_SHORT).show();
            return 0;
        }
    }
}
