package com.homework.a2013105031.earthquakeshelter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.URL;

public class EarthquakeInfoActivity extends AppCompatActivity {

    Handler handler = new Handler();  // 외부쓰레드 에서 메인 UI화면을 그릴때 사용

    ImageView imgView;
    TextView fcTpTextView;
    TextView locationTextView;
    TextView timeTextView;
    TextView remTextView;
    TextView latitudeTextView;
    TextView longitudeTextView;
    TextView mtTextView;

    private String location; // 위치
    private String time; // 시간(년월일시분)
    private String fcTp; // 통보 종류(미리 문자열로 해석해서 추가) 1 : 국내 지진정보 2 : 국외 지진정보 3 : 국내 지진통보 4 : 국외 지진통보 5 : 지진 정밀분석
    private String rem; // 참고사항
    private double latitude; // 위도
    private double longitude; // 경도
    private double mt; // 규모
    private String imgUrl; // 발생위치 이미지 url

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earthquake_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // 인텐트로부터 데이터 수신
        try {
            Intent intent = getIntent();
            location = intent.getStringExtra("loc");
            time = intent.getStringExtra("tmFc");
            fcTp = intent.getStringExtra("fcTp");
            rem = intent.getStringExtra("rem");
            imgUrl = intent.getStringExtra("imgUrl");
            latitude = intent.getDoubleExtra("lat", 0);
            longitude = intent.getDoubleExtra("lon", 0);
            mt = intent.getDoubleExtra("mt", 0);
        }
        catch (Exception e)
        {
            Log.d("info_activity", e.getMessage());
        }

        // 뷰 연결
        try {
            fcTpTextView = (TextView) findViewById(R.id.fcTp);
            timeTextView = (TextView) findViewById(R.id.tmFc);
            remTextView = (TextView) findViewById(R.id.rem);
            locationTextView = (TextView) findViewById(R.id.loc);
            latitudeTextView = (TextView) findViewById(R.id.lat);
            longitudeTextView = (TextView) findViewById(R.id.lon);
            mtTextView = (TextView) findViewById(R.id.mt);
        }
        catch (Exception e)
        {
            Log.d("info_activity", e.getMessage());
        }

        // 뷰에 정보 추가
        try {
            fcTpTextView.setText("통보 종류 : " + fcTp);
            timeTextView.setText("발표 시각 : " + time);
            remTextView.setText("참고 사항 : " + rem);
            locationTextView.setText("위치 : " + location);
            latitudeTextView.setText("위도 : " + Double.toString(latitude));
            longitudeTextView.setText("경도 : " + Double.toString(longitude));
            mtTextView.setText("규모 : " + Double.toString(mt));
        }
        catch (Exception e)
        {
            Log.d("info_activity", e.getMessage());
        }

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {    // 오래 거릴 작업을 구현한다
                // TODO Auto-generated method stub
                try{
                    final ImageView iv = (ImageView)findViewById(R.id.url_image);
                    URL url = new URL(imgUrl);
                    InputStream is = url.openStream();
                    final Bitmap bm = BitmapFactory.decodeStream(is);
                    handler.post(new Runnable() {
                        @Override
                        public void run() {  // 화면에 그려줄 작업
                            iv.setImageBitmap(bm);
                        }
                    });
                    iv.setImageBitmap(bm); //비트맵 객체로 보여주기
                }
                catch(Exception e){
                }
            }
        });
        t.start();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == R.id.action_button)
            finish();
        return super.onOptionsItemSelected(item);
    }


}
