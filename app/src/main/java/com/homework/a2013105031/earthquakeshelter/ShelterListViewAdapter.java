package com.homework.a2013105031.earthquakeshelter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by woochan on 2017-12-15.
 */

public class ShelterListViewAdapter extends BaseAdapter {
    private ArrayList<ShelterListViewItem> itemList = new ArrayList<>();

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final int position = i;
        final Context context = viewGroup.getContext();

        // "listview_item" Layout을 inflate하여 view 참조 획득.
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.listview_item_shelter, viewGroup, false);
        }

        // 화면에 표시될 View(Layout이 inflate된)으로부터 위젯에 대한 참조 획득
        ImageView iconImageView = (ImageView)view.findViewById(R.id.imageview_icon);
        TextView nameTextView = (TextView)view.findViewById(R.id.textview_name);
        TextView addressTextView = (TextView)view.findViewById(R.id.textview_address);

        // Data Set(listViewItemList)에서 position에 위치한 데이터 참조 획득
        ShelterListViewItem shelterListViewItem = itemList.get(position);

        // 아이템 내 각 위젯에 데이터 반영
        iconImageView.setImageDrawable(shelterListViewItem.getImage());
        nameTextView.setText(shelterListViewItem.getName());
        addressTextView.setText(shelterListViewItem.getAddress());

        return view;
    }

    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public ShelterListViewItem getItem(int i) {
        return itemList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public void addItem(Drawable image, String name, String address, double latitude, double longitude) {
        ShelterListViewItem shelterListViewItem = new ShelterListViewItem();

        shelterListViewItem.setImage(image);
        shelterListViewItem.setName(name);
        shelterListViewItem.setAddress(address);
        shelterListViewItem.setLatitude(latitude);
        shelterListViewItem.setLongitude(longitude);

        itemList.add(shelterListViewItem);
    }

    public void clearItemList() {
        itemList.clear();
    }
}
