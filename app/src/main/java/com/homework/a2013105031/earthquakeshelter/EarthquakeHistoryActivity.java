package com.homework.a2013105031.earthquakeshelter;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class EarthquakeHistoryActivity extends AppCompatActivity {
    TextView textView;

    ListView earthquakeHistoryListView;
    EarthquakeHistoryListViewAdapter earthquakeHistoryListViewAdapter;

    // 공공데이터 수집

    String END_TAG = "EarthquakeIndoors";
    boolean data_end = false;

    String urlData = "http://newsky2.kma.go.kr/service/ErthqkInfoService/EarthquakeReport"; // 실내구호소 공공데이터
    String urlKey = "%2F5R%2FXtmv1dOlX7j1JfJlYD%2FpCOQj7DX3%2BTqquhiRcUiwefRomwiVgvDYKAw7IwCY4DsYUDm2wjfh5BgSH3xwmA%3D%3D"; // 실내구호소 공공데이터 인증키
    int urlIndex = 1;
    String _url = urlData + "?KEY=" + urlKey;

    ArrayList<EarthquakeHistoryListViewItem> arrayList = new ArrayList<>();
    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earthquake_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // 테스트용 텍스트뷰
        textView = (TextView)findViewById(R.id.textview);
        //textView.setText(OpenDataReader.getDataByUrl());

        // 리스트뷰 설정
        earthquakeHistoryListView = (ListView)findViewById(R.id.listview_earthquake_history);
        earthquakeHistoryListViewAdapter = new EarthquakeHistoryListViewAdapter();
        earthquakeHistoryListView.setAdapter(earthquakeHistoryListViewAdapter);

        // 리스트뷰 터치 이벤트 추가
        earthquakeHistoryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(EarthquakeHistoryActivity.this, EarthquakeInfoActivity.class);

                intent.putExtra("fcTp", earthquakeHistoryListViewAdapter.getItem(i).getFcTp());
                intent.putExtra("tmFc", earthquakeHistoryListViewAdapter.getItem(i).getTime());
                intent.putExtra("rem", earthquakeHistoryListViewAdapter.getItem(i).getRem());
                intent.putExtra("loc", earthquakeHistoryListViewAdapter.getItem(i).getLocation());
                intent.putExtra("lat", earthquakeHistoryListViewAdapter.getItem(i).getLatitude());
                intent.putExtra("lon", earthquakeHistoryListViewAdapter.getItem(i).getLongitude());
                intent.putExtra("mt", earthquakeHistoryListViewAdapter.getItem(i).getMt());
                intent.putExtra("imgUrl", earthquakeHistoryListViewAdapter.getItem(i).getImgUrl());

                startActivity(intent);
            }
        });

        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == R.id.action_button)
            finish();
        return super.onOptionsItemSelected(item);
    }

    private void init() {
        dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage("데이터 수집 중...");
        dialog.show();
        new BackgroundTask().execute();
    }

    private class BackgroundTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... arg0) {
            // 한 페이지 당 1000개씩 3페이지의 실내구호소 데이터를 읽는다
            try {
                _url = urlData + "?serviceKey=" + urlKey + "&numOfRows=10&pageSize=10&pageNo=1&startPage=1&fromTmFc=20170101&toTmFc=20171231";
                URL url = new URL(_url);
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
                XmlPullParser parser = factory.newPullParser();
                parser.setInput(new BufferedInputStream(url.openStream()), "utf-8");

                String tag = null; // 태그

                String fcTp = null;
                String img = null;
                String loc = null;
                String lat = null;
                String lon = null;
                String mt = null;
                String rem = null;
                String tmFc = null;

                int type = parser.getEventType(); // 태그의 종류를 읽어온다
                while (type != XmlPullParser.END_DOCUMENT) {
                    tag = parser.getName(); // 태그 이름을 읽어온다
                    switch (type) {
                        case XmlPullParser.START_TAG: {
                            if (tag.equals("fcTp")) {
                                parser.next();
                                fcTp = parser.getText();
                                // 1 : 국내 지진정보 2 : 국외 지진정보 3 : 국내 지진통보 4 : 국외 지진통보 5 : 지진 정밀분석
                                if (fcTp.equals("1"))
                                    fcTp = "국내 지진정보";
                                else if (fcTp.equals("2"))
                                    fcTp = "국외 지진정보";
                                else if (fcTp.equals("3"))
                                    fcTp = "국내 지진통보";
                                else if (fcTp.equals("4"))
                                    fcTp = "국외 지진통보";
                                else if (fcTp.equals("5"))
                                    fcTp = "지진 정밀분석";
                                parser.next();
                            } else if (tag.equals("img")) {
                                parser.next();
                                img = parser.getText();
                                parser.next();
                            } else if (tag.equals("loc")) {
                                parser.next();
                                loc = parser.getText();
                                parser.next();
                            } else if (tag.equals("lat")) {
                                parser.next();
                                lat = parser.getText();
                                parser.next();
                            } else if (tag.equals("lon")) {
                                parser.next();
                                lon = parser.getText();
                                parser.next();
                            } else if (tag.equals("mt")) {
                                parser.next();
                                mt = parser.getText();
                                parser.next();
                            } else if (tag.equals("rem")) {
                                parser.next();
                                rem = parser.getText();
                                parser.next();
                            } else if (tag.equals("tmFc")) {
                                parser.next();
                                tmFc = parser.getText();
                                parser.next();
                            }
                            break;
                        }
                        case XmlPullParser.END_TAG: {
                            // 데이터 item 하나에 대해 컬럼값을 읽기 시작한다.
                            if (tag.equals("item")) {
                                publishProgress(fcTp, img, loc, lat, lon, mt, rem, tmFc);
                            }
                            break;
                        }
                    }
                    type = parser.next();
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.d("background_progress", e.getMessage());
            }


            return _url;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);

            EarthquakeHistoryListViewItem item = new EarthquakeHistoryListViewItem();
            item.setFcTp(values[0]);
            item.setImgUrl(values[1]);
            item.setLocation(values[2]);
            item.setLatitude(Double.parseDouble(values[3]));
            item.setLongitude(Double.parseDouble(values[4]));
            item.setMt(Double.parseDouble(values[5]));
            item.setRem(values[6]);
            item.setTime(values[7]);

            arrayList.add(item);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // 리스트뷰에 아이템 추가
            //shelterListViewAdapter.clearItemList();
            for (EarthquakeHistoryListViewItem item : arrayList) {
                item.setImage(getDrawable(R.drawable.earthquake));
                earthquakeHistoryListViewAdapter.addItem(item);
            }
            earthquakeHistoryListViewAdapter.notifyDataSetChanged();

            dialog.dismiss();
        }
    }
}
