package com.homework.a2013105031.earthquakeshelter;

import java.util.ArrayList;

public class quizdata {
    private ArrayList<String> QStringlist;
    private ArrayList<String> C1Stringlist;
    private ArrayList<String> C2Stringlist;
    private ArrayList<String> C3Stringlist;
    private ArrayList<String> C4Stringlist;
    private ArrayList<Integer> AStringlist;

    public quizdata()
    {
        QStringlist = new ArrayList<String>();
        C1Stringlist = new ArrayList<String>();
        C2Stringlist = new ArrayList<String>();
        C3Stringlist = new ArrayList<String>();
        C4Stringlist = new ArrayList<String>();
        AStringlist = new ArrayList<Integer>();

        QStringlist.add("집 안에 있을 경우 대응요령 중 올바르지 않은 것은?");
        C1Stringlist.add("지진발생 때는 진동에 의해 유리창이나 간판이 떨어질 수 있어 서둘러 밖으로 나가면 안 된다");
        C2Stringlist.add("문을 열어 출구를 확보해야 한다.");
        C3Stringlist.add("흔들리기 시작하면 바로 전기와 가스를 차단하고 출구로 나와야 한다.");
        C4Stringlist.add("탁자 아래로 들어가 흔들림이 멈출 때 까지 있는다.");
        AStringlist.add(3);

        QStringlist.add("건물내부에서 지진이 발생할 때 옳은 대처방법은?");
        C1Stringlist.add("엘리베이터를 타고 빠르게 건물 밖으로 나온다.");
        C2Stringlist.add("엘리베이터에 타고 있을 시 가능한 빨리 엘리베이터를 멈춰 내린 후 계단으로 밖으로 나온다.");
        C3Stringlist.add("화장실로 들어가서 지진이 멈추기를 기다린다.");
        C4Stringlist.add("주변의 유리가 많이 있는 곳을 지나간다.");
        AStringlist.add(2);

        QStringlist.add("길을 걷고 있을 때 지진이 발생한 경우 옳은 행동은?");
        C1Stringlist.add("보이는 큰 빌딩 근처로 간다.");
        C2Stringlist.add("공원으로 간다.");
        C3Stringlist.add("사람이 많은 곳에서 패닉에 빠져 가만히 있는다.");
        C4Stringlist.add("주변에 넓은 공간이 없으면 빠르게 그곳을 빠져 나온다.");
        AStringlist.add(2);
    }

    public ArrayList<String> getQStringlist()
    {
        return QStringlist;
    }
    public ArrayList<Integer> getAStringlist()
    {
        return AStringlist;
    }
    public ArrayList<String> getC1Stringlist()
    {
        return C1Stringlist;
    }
    public ArrayList<String> getC2Stringlist()
    {
        return C2Stringlist;
    }
    public ArrayList<String> getC3Stringlist()
    {
        return C3Stringlist;
    }
    public ArrayList<String> getC4Stringlist()
    {
        return C4Stringlist;
    }

}
