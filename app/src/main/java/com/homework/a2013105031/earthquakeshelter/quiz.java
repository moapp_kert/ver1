package com.homework.a2013105031.earthquakeshelter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class quiz extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        if(id == R.id.action_button)
            finish();
        return super.onOptionsItemSelected(item);
    }

    public void gotoquiz_ox(View v){
        Intent intent1 = new Intent(quiz.this, quiz_ox.class);
        startActivity(intent1);
    }

    public void gotoquiz_choice4(View v){
        Intent intent2 = new Intent(quiz.this, quiz_choice4.class);
        startActivity(intent2);
    }
}
